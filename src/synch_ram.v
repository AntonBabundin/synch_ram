// Design Name : synch_ram
// File Name   : synch_ram.v
// Function    : Synch RAM 1024 x 16 with init
// Coder       : Babundin Anton

module synch_ram
(
  input  wire                  clk, // clock
  input  wire           reset_rg_1, // Synch reset for rg_1
  input  wire           reset_rg_2, // Synch reset for rg_2
  input  wire            wr_enable, // Write enable for RAM
  input  wire            rd_enable, // Read enable for rg_2
  input  wire          byte_enable, // Signal enable for write byte
  input  wire [15:0]       data_in, // Input data
  input  wire [5:0]        address, // Address
  
  output wire [15:0] data_out       // Output data
  
);

  reg [15:0] ram [63:0]; //Declare RAM
  reg [5:0]  address_rg;    //Declare address register  
  
  wire [15:0]  data_in_ram; // Connect rg_1 and RAM
  wire [15:0] data_out_ram; // Connect RAM and rg_2
  
  initial 
    begin : INIT            //Initialization from file  initialization.txt
      $readmemb("../src/initialization.txt", ram,0,63);
    end 
  
  rg rg_1 (.clk(clk), .reset(reset_rg_1), .enable(byte_enable), .data_in_rg(data_in), .data_out_from_rg(data_in_ram));
  rg rg_2 (.clk(clk), .reset(reset_rg_2), .enable(rd_enable), .data_in_rg(data_out_ram), .data_out_from_rg(data_out));
  
  
  always @(posedge clk)
    begin
	  if(wr_enable == 1'b1)
        begin
		  ram[address] <= data_in_ram; // Write
        end
    end
  
  always @(posedge clk)
    begin
	  address_rg <= address;
	end

  assign data_out_ram = ram[address_rg];

endmodule 