// Design Name : synch_ram
// File Name   : rg.v
// Function    : register with enable and synch reset
// Coder       : Babundin Anton

module rg
(
  input wire clk,
  input wire reset,
  input wire enable,
  input wire [15:0] data_in_rg,
  
  output reg [15:0] data_out_from_rg
);

  
  always @(posedge clk)
    begin
      if(reset == 1'b1)
        begin
          data_out_from_rg <= 16'h0;
        end
      else
        begin
          if(enable == 1'b1)
            begin
              data_out_from_rg <= data_in_rg;
            end
        end
    end 

endmodule 