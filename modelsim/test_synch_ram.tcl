
# vlog - вызов компилятора verilog для компиляции нашего кода непосредственно в ModelSim, "- work" - указываем библиотеку, "-O0" - отключаем всю оптимизацию, чтобы видеть все объекты (на схеме, в списках и т.п.) в том составе, как они описаны 
# То есть мы компилируем наш файл и добавляем его в библиотеку work 
vlog -work work -O0 "../src/rg.v"
vlog -work work -O0 "../src/synch_ram.v"    

# tb
vlog -work work -O0 "../tb/tb_for_synch_ram.v"

# vsim - загружает новый дизайн в симулятор, далее путь к tb в ModelSim
#"-assertdebug" - подключаем утверждения для отладки ошибок($info, $error, $stop..)
# -L  Библиотека поиска для модулей, созданных на Verilog, указываеим библиотеку где лежат все наши модули
# "-msgmode" - команда переключает режимы отображения "сообщений"
# выводятся или в консоль или в графику или и туда и туда
# параметр ключа both значит и там и там
# -voptargs="+acc" для выборочной видимости объекта проектирования во время отладки
vsim work.tb_for_synch_ram -assertdebug -L work -msgmode both -voptargs="+acc"   
 
# добавляем наши сигналы 
# "add" - команда добавить  что-либо 
# wave - добавляем указанный сигнал на временную диаграмму, далее указываем модуль и какой сигнал добавляем 

add wave /tb_for_synch_ram/clk
add wave /tb_for_synch_ram/reset_rg_1
add wave /tb_for_synch_ram/reset_rg_2
add wave /tb_for_synch_ram/wr_enable
add wave /tb_for_synch_ram/rd_enable
add wave /tb_for_synch_ram/byte_enable
add wave /tb_for_synch_ram/data_in
add wave /tb_for_synch_ram/address
add wave /tb_for_synch_ram/data_out

# Шкала времени в пикосекундах
configure wave -timelineunits ps

# Запуск моделлирования
# -all - выполнение моделлирования до конца (в нашем случае до $stop)
run -all
