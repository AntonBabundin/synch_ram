// Design Name : synch_ram
// File Name   : tb_for_synch_ram.v
// Function    : testbech for synch_ram.v
// Coder       : Babundin Anton

`timescale 100 ps/ 1 ps

module tb_for_synch_ram();

  reg                  clk; 
  reg           reset_rg_1;  
  reg           reset_rg_2; 
  reg            wr_enable; 
  reg            rd_enable; 
  reg          byte_enable; 
  reg [15:0]       data_in; 
  reg [5:0]        address; 
  
  wire [15:0]  data_out;

  synch_ram i1 (.clk(clk), .reset_rg_1(reset_rg_1), .reset_rg_2(reset_rg_2), .wr_enable(wr_enable), .rd_enable(rd_enable), .byte_enable(byte_enable), .data_in(data_in), .address(address), .data_out(data_out));

  initial                                                
    begin
      clk = 0;
	  
	  #20
	  reset_rg_1 = 1'b1;
	  reset_rg_2 = 1'b1;
	  
	  #20
	  reset_rg_1 = 1'b0;
	  reset_rg_2 = 1'b0;
        
      #20                // Write data in address 111111
      byte_enable = 1'b1; 
      wr_enable = 1'b1;
      data_in = 16'hFFFF;
      address = 6'h3F;
        
      #20
      byte_enable = 1'b0;
      wr_enable = 1'b0;
        
      #20               //Read data from address 111111
      rd_enable = 1'b1; 
      address = 6'h3F;
        
      #25
      $stop;                       
    end                                                    
    always
      begin
        #5  
        clk =  ! clk;
      end
endmodule       